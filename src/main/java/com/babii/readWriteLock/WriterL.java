package com.babii.readWriteLock;

public class WriterL extends Thread {
    private ReadWriteStringBuilder addInfo;

    public WriterL(ReadWriteStringBuilder addInfo) {
        this.addInfo = addInfo;
    }

    @Override
    public void run() {
        String msg = "some msg";
        for(char c: msg.toCharArray()) {
            addInfo.addMsg(c);
            System.out.println(getName() + " putting -> " + c);
            try {
                Thread.sleep(500);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
