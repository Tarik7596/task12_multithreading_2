package com.babii.readWriteLock;

public class Task3 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        ReadWriteStringBuilder info = new ReadWriteStringBuilder(sb);
        WriterL writerL = new WriterL(info);
        ReaderLock readerLock = new ReaderLock(info);
        Thread t1 = new Thread(writerL);
        Thread t2 = new Thread(readerLock);
        t1.start();
        t2.start();
    }
}
