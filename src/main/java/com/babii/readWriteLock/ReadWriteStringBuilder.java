package com.babii.readWriteLock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteStringBuilder {
    private StringBuilder sb;
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public ReadWriteStringBuilder(StringBuilder sb) {
        this.sb = sb;
    }

    public void addMsg(char msg) {
        Lock wLock = readWriteLock.writeLock();
        wLock.lock();
        try {
            this.sb = sb.append(msg);
        } finally {
            wLock.unlock();
        }
    }

    public StringBuilder getMsg(char c) {
        Lock rLock = readWriteLock.readLock();
        rLock.lock();
        try {
            return sb;
        } finally {
            rLock.unlock();
        }
    }
}
