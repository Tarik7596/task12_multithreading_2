package com.babii.readWriteLock;

public class ReaderLock extends Thread {
    private ReadWriteStringBuilder getInfo;

    public ReaderLock(ReadWriteStringBuilder getInfo) {
        this.getInfo = getInfo;
    }

    @Override
    public void run() {
        int k = 0;
        String msg = "some msg";
        for (char c: msg.toCharArray()) {
            System.out.println(getName() + " getting-> " + getInfo.getMsg(c));
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
