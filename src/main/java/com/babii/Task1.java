package com.babii;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Task1 {
    private Lock buySomeproduct;
    private Lock cookFood;
    private String time;
    private int i = 0;

    Task1(Lock buySomeproduct, Lock cookFood, String time) {
        this.buySomeproduct = buySomeproduct;
        this.cookFood = cookFood;
        this.time = time;
    }

    public void buy() {
        if (buySomeproduct.tryLock()) {
            try {
                while (i < 1200000) i++;
                System.out.println("Buy all product - " + " " + i  + " " + LocalTime.now());
            } finally {
                buySomeproduct.unlock();
            }
        }
    }

    public void cook() {
        cookFood.lock();
        {
            try {
                while (i > 5) i--;
                System.out.println("We have " + i + " dishes " + LocalTime.now());
            } finally {

                cookFood.unlock();
            }
        }
    }


    public void timePrepared() {
        try {
            if (cookFood.tryLock(5, TimeUnit.SECONDS)) {
                try {
                    System.out.println("Cook time " + time + " " + LocalTime.now());

                } finally {
                    cookFood.unlock();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new SynhronizedRun(), "t1");
        Thread t2 = new Thread(new SynhronizedRun(), "t2");
        Thread t3 = new Thread(new SynhronizedRun(), "t3");
        t1.start();
        t2.start();
        t3.start();
    }
}

class SynhronizedRun extends Thread {
    Lock lock = new ReentrantLock();
    Task1 task1 = new Task1(lock, lock, "two hours");

    @Override
    public void run() {
        try {
            if(lock.tryLock(4,TimeUnit.SECONDS)) {
                task1.buy();
                task1.cook();
                task1.timePrepared();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
