package com.babii.blockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class QCommunicate {
    public static void main(String[] args) {
        BlockingQueue<String> bq = new ArrayBlockingQueue<>(512);
        String msg = "Hello, I'm putting some msg!";
        QReader qReader = new QReader(bq);
        QWriter qWriter = new QWriter(bq,msg);
        new Thread(qReader).start();
        new Thread(qWriter).start();
    }
}
