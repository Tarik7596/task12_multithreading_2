package com.babii.blockingQueue;

import java.util.concurrent.BlockingQueue;

public class QWriter extends Thread {
    BlockingQueue bqOut ;
    String msg;

    public QWriter(BlockingQueue bqOut, String msg) {
        this.bqOut=bqOut;
        this.msg = msg;
    }

    @Override
    public void run() {
        while(true) {
            try {
                bqOut.put(msg);
                Thread.sleep(10000);
                System.out.println();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
